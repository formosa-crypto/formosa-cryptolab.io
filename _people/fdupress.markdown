---
name: François Dupressoir
institution: University of Bristol
website: https://fdupress.net

projects: EasyCrypt
---

[François](https://fdupress.net) is a core member of the EasyCrypt team. His
work focuses on developing libraries of ready-to-use proof blocks and
constructions that ease the development of machine-checked cryptographic proofs
of all scales and intricacies, from primitives to protocols.
