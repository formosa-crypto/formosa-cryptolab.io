---
name: Benjamin Grégoire
institution: Inria, centre d'Université Côte d'Azur
website: http://www-sop.inria.fr/members/Benjamin.Gregoire/

projects: EasyCrypt, Jasmin
---
