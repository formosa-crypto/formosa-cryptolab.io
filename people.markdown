---
layout: page
title: People

permalink: /people/
---

## Current members

{% for person in site.people %}
- [**{{ person.name }}**]({{ person.website }}) ({{ person.institution }})<br/>
  {% if person.projects %} Projects: {{ person.projects }}<br/> {% endif %}
  {{ person.content | markdownify }}
{% endfor %}

## Former members

{% for person in site.former_members %}
- [**{{ person.name }}**]({{ person.website }}) ({{ person.institution }})<br/>
  {% if person.projects %} Projects: {{ person.projects }}<br/> {% endif %}
  {{ person.content | markdownify }}
{% endfor %}
