---
name: A solid theory for post-quantum cryptography
funder: Nederlandse Organisatie voor Wetenschappelijk Onderzoek (NWO)
programme: NWO-Talentprogramma Vidi ENW 2019

number: VI.Vidi.193.066

date: 2021-05-01
end: 2026-05-03
---

Vidi Grant, Andreas Hülsing
