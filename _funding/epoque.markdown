---
name: Engineering Post-Quantum Cryptography (EPOQUE)
funder: European Research Council
programme: 

number: ERC StG 805031

date: 2018-10-01
end: 2023-12-31
---

ERC Starting Grant, Peter Schwabe
