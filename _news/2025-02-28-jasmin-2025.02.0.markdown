---
layout: post

title: Release 2025.02.0 of the Jasmin compiler
date: 2025-02-28

summary: >-
  A new major version of Jasmin is available.
linkback: >-
  Read the announcement.
---

A new major version of the Jasmin compiler has just been released. It contains
two big changes, together with fixes and additions whose details can be found
in the
[CHANGELOG](https://github.com/jasmin-lang/jasmin/blob/v2025.02.0/CHANGELOG.md).
Both big changes are highlighted below.

## Support RISC-V 32IM as target architecture

The Jasmin language and tools now offer experimental support for the RISC-V
architecture. Use `jasminc -arch riscv` or `jasmin2ec --arch=riscv` to use it.

## Overhaul of the EasyCrypt extraction

Extraction of Jasmin programs to EasyCrypt is now available as a separate
`jasmin2ec` tool. It uses a new set of theories in `eclib/` for extracting
array operations and supports a new extraction for leakage based on local
variables.
