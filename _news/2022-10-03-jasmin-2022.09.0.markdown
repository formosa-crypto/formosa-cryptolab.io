---
layout: post

title: Release 2022.09.0 of the Jasmin compiler
date: 2022-10-03

summary: >-
  A new version of Jasmin is available.
linkback: >-
  Read the announcement.
---

A new version of the Jasmin compiler has just been released. It features many
fixes and improvements compared to the previous 2022.04.0 version. Their full
list can be found in the
[changelog](https://github.com/jasmin-lang/jasmin/blob/v2022.09.0/CHANGELOG.md#jasmin-2022090).

A major new feature of this version is the availability of the `#randombytes`
system call, that can fill a byte array with unspecified data. This is
particularly useful for implementations that rely on rejection sampling and
cannot easily take random data as input.

This version is still compatible with [EasyCrypt 2022.04](https://github.com/EasyCrypt/easycrypt/releases/tag/r2022.04).

Thanks to all the contributors and to the many users, whose constructive
feedback is highly appreciated.
