---
layout: post

title: Release 2023.06.2 of the Jasmin compiler
date: 2023-12-22

summary: >-
  A new minor version of Jasmin is available.
linkback: >-
  Read the announcement.
---

A new minor version of the Jasmin compiler has just been released. It includes
a few fixes and improvements, related in particlar to the still experimental
support for ARMv7 architecture.

Relevant details can be found through the
[CHANGELOG](https://github.com/jasmin-lang/jasmin/blob/v2023.06.2/CHANGELOG.md).
