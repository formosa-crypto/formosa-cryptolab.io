---
layout: post

title: Swarn Priya wins L'Oréal-UNESCO 2023 prize
date: 2023-10-09

summary: >-
  [Swarn Priya](https://swarnpriya.github.io) won the L'Oréal-UNESCO
  "Pour les Femmes et la Science" Young Talents France 2023 Award.
linkback: >-
  Read more.
---

[Swarn Priya](https://swarnpriya.github.io) won the L'Oréal-UNESCO
"Pour les Femmes et la Science" Young Talents France 2023 Award.
[Here](https://www.fondationloreal.com/sites/default/files/2023-10/cp_fondation_l_oreal_jeunes_talents_2023.pdf)
is the official announcement (in French).

More information on
[Inria's website](https://www.inria.fr/en/certifying-correctness-security-and-reliability-software-swarn-priya-wins-loreal-unesco-2023-prize).
