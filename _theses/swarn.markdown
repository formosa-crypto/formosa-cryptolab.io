---
title: Formally computer-verified protections against timing-based side-channel attacks
authors: Swarn Priya
website: https://hal.science/tel-04331805v1/document
year: 2023
---
