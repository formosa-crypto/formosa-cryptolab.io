---
layout: page
title: News

permalink: /news/
---

{% for item in site.news reversed %}
- [**{{ item.title }}**]({{ item.url | relative_url }}) ({{ item.date | date: '%B' }} {{ item.date | date : '%d' | plus:'0' }}{{ item.date | date : ', %Y' }})<br/>
  {% capture link %} [{{ item.linkback }}]({{ item.url | relative_url }}){% endcapture %}
  {{ item.summary | append:link | markdownify | strip_newlines }}
{% endfor %}
