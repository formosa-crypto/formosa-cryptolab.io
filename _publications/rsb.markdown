---
title: Protecting Cryptographic Code Against Spectre-RSB
year: 2024
authors: Santiago Arranz Olmos, Gilles Barthe, Chitchanok Chuengsatiansup, Benjamin Grégoire, Vincent Laporte, Tiago Oliveira, Peter Schwabe, Yuval Yarom, Zhiyuan Zhang
website: https://eprint.iacr.org/2024/1070
conference: ASPLOS, 2025

---
