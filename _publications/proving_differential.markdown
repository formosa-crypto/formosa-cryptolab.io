---
title: "Proving Differential Privacy via Probabilistic Couplings"
year: 2016
authors: Gilles Barthe, Marco Gaboardi, Benjamin Grégoire, Justin Hsu, and Pierre-Yves Strub
website: https://arxiv.org/abs/1601.05047
conference: LICS, 2019

---
