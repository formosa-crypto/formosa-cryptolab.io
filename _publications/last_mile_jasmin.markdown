---
title: "The Last Mile: High-Assurance and High-Speed Cryptographic Implementations"
year: 2019
authors: José Bacelar Almeida, Manuel Barbosa, Gilles Barthe, Benjamin Grégoire, Adrien Koutsos, Vincent Laporte, Tiago Oliveira, and Pierre-Yves Strub
website: https://arxiv.org/abs/1904.04606
conference: S&P, 2019

---
