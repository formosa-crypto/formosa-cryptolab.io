---
title: High-assurance zeroization
year: 2023
authors: Santiago Arranz Olmos, Gilles Barthe, Ruben Gonzalez, Benjamin Grégoire, Vincent Laporte, Jean-Christophe Léchenet, Tiago Oliveira, Peter Schwabe
website: https://eprint.iacr.org/2023/1713
conference: IACR, 2023

---
