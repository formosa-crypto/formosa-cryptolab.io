---
title: Preservation of Speculative Constant-Time by Compilation
year: 2024
authors: Santiago Arranz Olmos, Gilles Barthe, Lionel Blatter, Benjamin Grégoire, Vincent Laporte
website: https://eprint.iacr.org/2024/1203
conference: POPL, 2025

---
