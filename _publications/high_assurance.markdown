---
title: High-Assurance Cryptography in the Spectre Era
year: 2020
authors: Gilles Barthe, Sunjay Cauligi, Benjamin Grégoire, Adrien Koutsos, Kevin Liao, Tiago Oliveira, Swarn Priya, Tamara Rezk, and Peter Schwabe
website: https://eprint.iacr.org/2020/1104
conference: S&P, 2020

---
