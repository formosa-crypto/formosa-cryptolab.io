---
title: "Zero-Knowledge in EasyCrypt"
year: 2023
authors: Denis Firsov and Dominique Unruh
website: https://firsov.ee/zk-in-easycrypt/
conference: CSF, 2023
---
